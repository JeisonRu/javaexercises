import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		
		int senha = 2002;
		System.out.print("Digite sua senha: ");
		int tentativa = sc.nextInt();
		while (tentativa != senha) {
			System.out.print("Senha inv�lida. Tente novamente: ");
			tentativa = sc.nextInt();
		}
		System.out.println("Acesso permitido.");
		System.out.println();
		
		System.out.print("Informe o primeiro ponto: ");
		int ponto1 = sc.nextInt();
		System.out.print("Informe o segundo ponto: ");
		int ponto2 = sc.nextInt();
		while (ponto1 != 0 && ponto2 != 0) {
			if (ponto1 > 0 && ponto2 > 0) {
				System.out.println("primeiro");
			} else if (ponto1 > 0 ) {
				System.out.println("quarto");
			} else if (ponto2 > 0) {
				System.out.println("segundo");
			} else {
				System.out.println("terceiro");
			}
			System.out.print("Informe o primeiro ponto: ");
			ponto1 = sc.nextInt();
			System.out.print("Informe o segundo ponto: ");
			ponto2 = sc.nextInt();
		}
		System.out.println();
		
		int alcool = 0;
		int gasolina = 0;
		int diesel = 0;
		System.out.print("Informe a op��o de combust�vel: ");
		int opcao = sc.nextInt();
		while (opcao != 4) {
			switch (opcao) {
			case 1: alcool += 1; break;
			case 2: gasolina += 1; break;
			case 3: diesel += 1; break;
			default: 
			}
			System.out.print("Informe a op��o de combust�vel: ");
			opcao = sc.nextInt();
		}
		System.out.println("Muito obrigado:");
		System.out.printf("Alcool: %d\n", alcool);
		System.out.printf("Gasolina: %d\n", gasolina);
		System.out.printf("Diesel: %d\n", diesel);
		
		sc.close();

	}

}
