package entities;

public class Student {
	
	private String name;
	private double firstGrade = 0.0;
	private double secondGrade = 0.0;
	private double thirdGrade = 0.0;
	
	public Student(String name, double firstGrade, double secondGrade, double thirdGrade) {
		this.name = name;
		this.firstGrade = firstGrade;
		this.secondGrade = secondGrade;
		this.thirdGrade = thirdGrade;
	}

	public Student() {
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getFirstGrade() {
		return firstGrade;
	}

	public void setFirstGrade(double firstGrade) {
		this.firstGrade = firstGrade;
	}

	public double getSecondGrade() {
		return secondGrade;
	}

	public void setSecondGrade(double secondGrade) {
		this.secondGrade = secondGrade;
	}

	public double getThirdGrade() {
		return thirdGrade;
	}

	public void setThirdGrade(double thirdGrade) {
		this.thirdGrade = thirdGrade;
	}
	
	public double finalGrade() {
		return this.firstGrade * .3 + this.secondGrade * .35 + this.thirdGrade * .35;
	}
	
	public void result() {
		if (this.finalGrade() >= 60) {
			System.out.println("PASS");;
		} else {
			System.out.println("FAILED");
			System.out.printf("MISSING %.2f POINTS", 60 - this.finalGrade());
		}
	}
	
	

}
