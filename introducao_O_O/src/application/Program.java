package application;

import java.util.Locale;
import java.util.Scanner;

import entities.Employee;
import entities.Rectangle;
import entities.Student;

public class Program {

	public static void main(String[] args) {
		
		Locale.setDefault(Locale.US);
		Scanner sc = new Scanner(System.in);
		
		Rectangle rec = new Rectangle();
		System.out.print("Informe largura e altura do ret�ngulo: ");
		rec.setWidth(sc.nextDouble());
		rec.setHeight(sc.nextDouble());
		System.out.println(String.format("%.2f", rec.area()));
		System.out.println(String.format("%.2f", rec.perimeter()));
		System.out.println(String.format("%.2f", rec.diagonal()));
		System.out.println();
		
		Employee emp = new Employee();
		System.out.print("Informe nome, sal�rio bruto e imposto: ");
		sc.nextLine();
		emp.setName(sc.nextLine());
		emp.setGrossSalary(sc.nextDouble());
		emp.setTax(sc.nextDouble());
		System.out.println(emp);
		System.out.print("Informe o percentual de aumento do sal�rio: ");
		double percentage = sc.nextDouble() / 100;
		emp.increaseSalary(percentage);
		System.out.println(emp);
		System.out.println();
		
		Student stu = new Student();
		System.out.print("Informe o nome e as tr�s notas trimestrais ");
		sc.nextLine();
		stu.setName(sc.nextLine());
		stu.setFirstGrade(sc.nextDouble());
		stu.setSecondGrade(sc.nextDouble());
		stu.setThirdGrade(sc.nextDouble());
		System.out.println("FINAL GRADE = " + stu.finalGrade());
		stu.result();
		
		
		sc.close();

	}

}
