package application;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import entities.Product;

public class Program {

	public static void main(String[] args) {
		
		Locale.setDefault(Locale.US);
		
		List<Product> list = new ArrayList<>();
		
		list.add(new Product("Tv", 900.0));
		list.add(new Product("Mouse", 50.0));
		list.add(new Product("Tablet", 450.0));
		list.add(new Product("Hd case", 80.90));
		
		list.removeIf(x -> x.getPrice() < 300.0);
		
		for (Product p : list) {
			System.out.println(p);
		}

	}

}
