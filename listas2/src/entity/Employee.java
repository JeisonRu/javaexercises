package entity;

public class Employee {
	
	private Integer id;
	private String nome;
	private Double salario;
	
	public Employee(Integer id, String nome, Double salario) {
		this.id = id;
		this.nome = nome;
		this.salario = salario;
	}
	
	public Integer getId() {
		return id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Double getSalario() {
		return salario;
	}

	public void aumentarSalario(Double percentual) {
		this.salario *= (1 + percentual/100);
	}

	@Override
	public String toString() {
		return String.format("%d, %s, %.2f", id, nome, salario);
	}
	
	

}
