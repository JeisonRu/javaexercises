package appllication;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Scanner;

import entity.Employee;

public class Program {

	public static void main(String[] args) {

		Locale.setDefault(Locale.US);
		Scanner sc = new Scanner(System.in);

		System.out.print("Quantos funcion�rios foram contratados? ");
		int quantidade = sc.nextInt();

		List<Employee> funcionarios = new ArrayList<>();

		for (int i = 0; i < quantidade; i++) {
			int id = sc.nextInt();
			sc.nextLine();
			String nome = sc.nextLine();
			double salario = sc.nextDouble();
			funcionarios.add(new Employee(id, nome, salario));
		}
		System.out.println();

		int id;
		do {
			System.out.print("Digite o id do funcion�rio com aumento de sal�rio: ");
			id = sc.nextInt();
			if (hasId(funcionarios, id) == -1) {
				System.out.println("Este id n�o existe");
			}
		} while (hasId(funcionarios, id) == -1);
		System.out.print("Qual ser� o aummento concedido? ");
		double percentual = sc.nextDouble();
		funcionarios.get(hasId(funcionarios, id)).aumentarSalario(percentual);

		for (Employee func : funcionarios) {
			System.out.println(func);
		}

		sc.close();

	}

	public static Integer hasId(List<Employee> list, int id) {
		for (int i = 0; i < list.size(); i++) {
			if (list.get(i).getId() == id) {
				return i;
			}
		}
		return -1;
	}

}
