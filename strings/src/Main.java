import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		
		System.out.print("Digite um nome: ");
		String nome = sc.nextLine();
		
		System.out.println(nome.toLowerCase());
		System.out.println(nome.toUpperCase());
		System.out.println(nome.trim());
		System.out.println(nome.substring(2));
		System.out.println(nome.substring(2, 5));
		System.out.println(nome.replace('a', 'e'));
		System.out.println(nome.replace("ard", "ore"));
		System.out.println(nome.indexOf('a'));
		System.out.println(nome.lastIndexOf('o'));
		System.out.println(nome.split(" ")[0]);
		
		
		sc.close();

	}

}
