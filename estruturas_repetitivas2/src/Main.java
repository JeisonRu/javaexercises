import java.util.Locale;
import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		Locale.setDefault(Locale.US);
		
		System.out.print("Informe um n�mero entre 1 e 1000: ");
		int num = sc.nextInt();
		for (int i=1; i<=num; i+=2) {
			System.out.println(i);
		}
		System.out.println();
		
		System.out.print("Informe a quantidade de valores: ");
		num = sc.nextInt();
		int in = 0;
		int out = 0;
		for (int i=0; i<num; i++) {
			System.out.print("Informe um n�mero: ");
			int var = sc.nextInt();
			if (var >= 10 && var <= 20) {
				in++;
			} else {
				out++;
			}
		}
		System.out.printf("%d in\n", in);
		System.out.printf("%d out\n", out);
		System.out.println();
		
		System.out.print("Informe a quantidade de casos de teste: ");
		num = sc.nextInt();
		for (int i=0; i<num; i++) {
			System.out.print("Informe o primeiro valor: ");
			double valor1 = sc.nextDouble();
			System.out.print("Informe o segundo valor: ");
			double valor2 = sc.nextDouble();
			System.out.print("Informe o terceiro valor: ");
			double valor3 = sc.nextDouble();
			double media = (valor1 * 2 + valor2 * 3 + valor3 * 5) / 10;
			System.out.printf("%.1f\n", media);
		}
		System.out.println();
		
		System.out.print("Informe a quantidade de casos de teste: ");
		num = sc.nextInt();
		for (int i=0; i<num; i++) {
			System.out.print("Informe o primeiro valor: ");
			double valor1 = sc.nextDouble();
			System.out.print("Informe o segundo valor: ");
			double valor2 = sc.nextDouble();
			if (valor2 == 0) {
				System.out.println("Divis�o imposs�vel");
			} else {
				System.out.println(valor1 / valor2);
			}
		}
		System.out.println();
		
		System.out.print("Informe o n�mero do fatorial a ser calculado: ");
		num = sc.nextInt();
		if (num == 0 || num == 1) {
			System.out.println(1);
		} else {
			int fatorial = 1;
			for (int i=2; i<=num; i++) {
				fatorial *= i;
			}
			System.out.println(fatorial);
		}
		System.out.println();
		
		System.out.print("Informe um n�mero inteiro: ");
		num = sc.nextInt();
		for (int i=1; i<=num; i++) {
			if (num % i == 0) {
				System.out.println(i);
			}
		}
		System.out.println();
		
		System.out.print("Informe um n�mero inteiro: ");
		num = sc.nextInt();
		for (int i=1; i<=num; i++) {
			System.out.print(i + " ");
			System.out.print(i * i + " ");
			System.out.println((int) Math.pow(i, 3.0));
		}
		System.out.println();
		
		
		
		sc.close();

	}

}
