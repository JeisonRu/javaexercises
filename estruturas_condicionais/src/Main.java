import java.util.Locale;
import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		
		System.out.print("Digite um n�mero inteiro: ");
		int num = sc.nextInt();
		if (num < 0) {
			System.out.println("NEGATIVO");
		} else {
			System.out.println("N�O NEGATIVO");
		}
		System.out.println();
		
		System.out.print("Digite um n�mero inteiro: ");
		num = sc.nextInt();
		if (num % 2 == 0) {
			System.out.println("PAR");
		} else {
			System.out.println("IMPAR");
		}
		System.out.println();
		
		System.out.print("Digite um n�mero inteiro: ");
		int a = sc.nextInt();
		System.out.print("Digite outro n�mero inteiro: ");
		int b = sc.nextInt();
		if (a > b) {
			if (a % b == 0) {
				System.out.println("S�o M�ltiplos");
			} else {
				System.out.println("N�o s�o M�ltiplos");
			}
		} else {
			if (b % a == 0) {
				System.out.println("S�o M�ltiplos");
			} else {
				System.out.println("N�o s�o M�ltiplos");
			}
		}
		System.out.println();
		
		System.out.print("Digite a hora inicial: ");
		int horaInicial = sc.nextInt();
		System.out.print("Digite a hora final: ");
		int horaFinal = sc.nextInt();
		if (horaInicial == horaFinal) {
			System.out.println("O jogo durou 24 hora(s)\n");
		} else if (horaFinal > horaInicial) {
			System.out.printf("O jogo durou %d hora(s)\n", horaFinal - 
					horaInicial);
		} else {
			System.out.printf("O jogo durou %d hora(s)\n", 24 + horaFinal - 
					horaInicial);
		}
		System.out.println();
		
		Locale.setDefault(Locale.US);
		double cachorroQuente = 4.0;
		double xSalada = 4.5;
		double xBacon = 5.0;
		double torradaSimples = 2.0;
		double refrigerante = 1.5;
		System.out.print("Digite o c�digo do �tem: ");
		int codigo = sc.nextInt();
		System.out.print("Digite a quantidade do �tem: ");
		int quantidade = sc.nextInt();
		switch (codigo) {
		case 1: System.out.printf("Total: R$ %.2f\n", cachorroQuente * quantidade); break;
		case 2: System.out.printf("Total: R$ %.2f\n", xSalada * quantidade); break;
		case 3: System.out.printf("Total: R$ %.2f\n", xBacon * quantidade); break;
		case 4: System.out.printf("Total: R$ %.2f\n", torradaSimples * quantidade); break;
		default: System.out.printf("Total: R$ %.2f\n", refrigerante * quantidade);
		}
		System.out.println();
		
		System.out.print("Digite um valor entre 0 e 100: ");
		double valor = sc.nextDouble();
		if (valor < 0 || valor > 100) {
			System.out.println("Fora do intervalo\n\n");
		} else if (valor <= 25) {
			System.out.println("Intervalo [0, 25]");
		} else if (valor <= 50) {
			System.out.println("Intervalo (25, 50]");
		} else if (valor <= 75) {
			System.out.println("Intervalo (50, 75]");
		} else {
			System.out.println("Intervalo (75, 100]");
		}
		System.out.println();
		
		System.out.print("Digite um valor: ");
		double valor1 = sc.nextDouble();
		System.out.print("Digite outro valor: ");
		double valor2 = sc.nextDouble();
		if (valor1 == 0 && valor2 == 0) {
			System.out.println("Origem");
		} else if (valor1 == 0) {
			System.out.println("Eixo X");
		} else if (valor2 == 0) {
			System.out.println("Eixo Y");
		} else if (valor1 > 0 && valor2 > 0) {
			System.out.println("Q1");
		} else if (valor1 > 0) {
			System.out.println("Q4");
		} else if (valor2 > 0) {
			System.out.println("Q2");
		} else {
			System.out.println("Q3");
		}
		System.out.println();
		
		System.out.print("Digite o valor do sal�rio: ");
		double salario = sc.nextDouble();
		if (salario <= 2000) {
			System.out.println("Isento");
		} else if (salario <= 3000) {
			double imposto = (salario - 2000) * 0.08;
			System.out.printf("R$ %.2f", imposto);
		} else if (salario <= 4500) {
			double imposto = 1000 * 0.08 + (salario - 3000) * 0.18;
			System.out.printf("R$ %.2f", imposto);
		} else {
			double imposto = 1000 * 0.08 + 1500 * 0.18 + (salario - 4500) * 0.28;
			System.out.printf("R$ %.2f", imposto);
		}
		
		sc.close();

	}

}
