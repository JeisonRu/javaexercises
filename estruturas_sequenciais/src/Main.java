import java.util.Locale;
import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		
		System.out.print("Digite um n�mero inteiro: ");
		int x = sc.nextInt();
		System.out.print("Digite outro n�mero inteiro: ");
		int y = sc.nextInt();
		int res = x + y;
		System.out.printf("SOMA: %d\n\n", res);
		
		System.out.print("Digite o valor do raio: ");
		double raio = sc.nextDouble();
		double area = Math.PI * Math.pow(raio, 2.0);
		System.out.printf("A: %.4f\n\n", area);
		
		System.out.print("Digite o primeiro n�mero inteiro: ");
		int a = sc.nextInt();
		System.out.print("Digite o segundo n�mero inteiro: ");
		int b = sc.nextInt();
		System.out.print("Digite o terceiro n�mero inteiro: ");
		int c = sc.nextInt();
		System.out.print("Digite o quarto n�mero inteiro: ");
		int d = sc.nextInt();
		res = a * b - c * d;
		System.out.println("DIFERENCA = " + res + "\n");
		
		System.out.print("Digite o n�mero do funcion�rio: ");
		int funcionario = sc.nextInt();
		System.out.print("Digite o n�mero de horas trabalhadas: ");
		double horas = sc.nextDouble();
		System.out.print("Digite o valor da hora trabalhada: ");
		double valor = sc.nextDouble();
		double salario = horas * valor;
		Locale.setDefault(Locale.US);
		System.out.println("NUMBER = " + funcionario);
		System.out.printf("SALARY = U$ %.2f\n\n", salario);
		
		System.out.print("Digite o c�digo da pe�a:");
		String peca1 = sc.next();
		System.out.print("Digite o n�mero de pe�as: ");
		int npeca1 = sc.nextInt();
		System.out.print("Digite o valor da pe�a: ");
		double vpeca1 = sc.nextDouble();
		System.out.print("Digite o c�digo da pe�a:");
		String peca2 = sc.next();
		System.out.print("Digite o n�mero de pe�as: ");
		int npeca2 = sc.nextInt();
		System.out.print("Digite o valor da pe�a: ");
		double vpeca2 = sc.nextDouble();
		double total = npeca1 * vpeca1 + npeca2 * vpeca2;
		System.out.printf("VALOR A PAGAR: R$ %.2f\n\n", total);
		
		System.out.print("Digite o primeiro n�mero: ");
		double val1 = sc.nextDouble();
		System.out.print("Digite o segundo n�mero: ");
		double val2 = sc.nextDouble();
		System.out.print("Digite o terceiro n�mero: ");
		double val3 = sc.nextDouble();
		double aTriangulo = (val1 / 2) * val3;
		System.out.printf("TRIANGULO: %.3f\n", aTriangulo);
		double aCirculo = Math.PI * Math.pow(val3, 2.0);
		System.out.printf("CIRCULO: %.3f\n", aCirculo);
		double aTrapezio = ((val1 + val2) /2) * val3;
		System.out.printf("TRAPEZIO: %.3f\n", aTrapezio);
		double aQuadrado = val2 * val2;
		System.out.printf("QUADRADO: %.3f\n", aQuadrado);
		double aRetangulo = val1 * val2;
		System.out.printf("RETANGULO: %.3f\n", aRetangulo);
		
		
		sc.close();

	}

}
