package application;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Program {

	public static void main(String[] args) {
		
		List<String> nomes = new ArrayList<>();
		
		nomes.add("Maria");
		nomes.add("Alex");
		nomes.add("Bob");
		nomes.add("Anna");
		nomes.add(2, "Marco");
		
		System.out.println(nomes.size());
		for (String nome : nomes) {
			System.out.println(nome);
		}
		System.out.println("-------------------");
		
		nomes.removeIf(x -> x.charAt(0) == 'M');
		for (String nome : nomes) {
			System.out.println(nome);
		}
		System.out.println("-------------------");
		
		System.out.println("Index of Bob: " + nomes.indexOf("Bob"));
		System.out.println("-------------------");
		
		List<String> resultado = nomes.stream().filter(x -> x.charAt(0) == 'A').collect(Collectors.toList());
		for (String res : resultado) {
			System.out.println(res);
		}
		System.out.println("-------------------");
		
		String nome = nomes.stream().filter(x -> x.charAt(0) == 'A').findFirst().orElse(null);
		System.out.println(nome);
		

	}

}
