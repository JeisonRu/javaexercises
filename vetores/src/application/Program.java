package application;

import java.util.Scanner;

import entity.Quarto;

public class Program {

	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		
		System.out.print("Quantos quartos ser�o alugados? ");
		int quantidade = sc.nextInt();
		Quarto[] pousada = new Quarto[10];
		
		for (int i=0; i<quantidade; i++) {
			System.out.println("Aluguel #" + (i + 1));
			sc.nextLine();
			String nome = sc.nextLine();
			String email = sc.next();
			int quarto = sc.nextInt();
			pousada[quarto] = new Quarto(nome, email);
		}
		
		for (int i=0; i<pousada.length; i++) {
			if (pousada[i] != null) {
				System.out.println(i + ": " + pousada[i]);
			}
		}
		
		sc.close();

	}

}
