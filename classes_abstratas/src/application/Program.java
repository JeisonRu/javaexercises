package application;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Scanner;

import entities.Contribuinte;
import entities.PessoaFisica;
import entities.PessoaJuridica;

public class Program {

	public static void main(String[] args) {
		
		Locale.setDefault(Locale.US);
		Scanner sc = new Scanner(System.in);
		
		List<Contribuinte> contribuintes = new ArrayList<>();
		
		System.out.print("Enter the number of tax payers: ");
		int number = sc.nextInt();
		
		for (int i = 1; i <= number; i++) {
			System.out.println("Tax payer #" + i + " data:");
			System.out.print("Individual or company (i/c): ");
			char option = sc.next().charAt(0);
			System.out.print("Name: ");
			sc.nextLine();
			String nome = sc.nextLine();
			System.out.print("Anual income: ");
			Double renda = sc.nextDouble();
			if (option == 'i') {
				System.out.print("Health expenditures: ");
				Double gastosSaude = sc.nextDouble();
				contribuintes.add(new PessoaFisica(nome, renda, gastosSaude));
			} else {
				System.out.print("Number of employees: ");
				int funcionarios = sc.nextInt();
				contribuintes.add(new PessoaJuridica(nome, renda, funcionarios));
			}
		}
		
		System.out.println();
		System.out.println("TAXES PAID:");
		for (Contribuinte con : contribuintes) {
			System.out.println(con.taxTag());
		}
		
		System.out.println();
		double sum = 0.0;
		for (Contribuinte con : contribuintes) {
			sum += con.pagarImposto();
		}
		System.out.println("TOTAL TAXES: $ " + String.format("%.2f", sum));
		
		sc.close();

	}

}
