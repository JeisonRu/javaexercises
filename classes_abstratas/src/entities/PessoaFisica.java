package entities;

public class PessoaFisica extends Contribuinte {
	
	private Double gastosSaude;

	public PessoaFisica(String nome, Double renda, Double gastosSaude) {
		super(nome, renda);
		this.gastosSaude = gastosSaude;
	}

	public Double getGastosSaude() {
		return gastosSaude;
	}

	public void setGastosSaude(Double gastosSaude) {
		this.gastosSaude = gastosSaude;
	}

	@Override
	public Double pagarImposto() {
		if (renda < 20000) {
			return renda * .15 - gastosSaude * .5;
		}
		return renda * .25 - gastosSaude * .5;
	}

}
