package entities;

public class PessoaJuridica extends Contribuinte {
	
	private Integer funcionarios;

	public PessoaJuridica(String nome, Double renda, Integer funcionarios) {
		super(nome, renda);
		this.funcionarios = funcionarios;
	}

	public Integer getFuncionarios() {
		return funcionarios;
	}

	public void setFuncionarios(Integer funcionarios) {
		this.funcionarios = funcionarios;
	}

	@Override
	public Double pagarImposto() {
		if (funcionarios > 10) {
			return renda * .14;
		}
		return renda * .16;
	}

}