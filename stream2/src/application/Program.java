package application;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

import entities.Product;

public class Program {

	public static void main(String[] args) {
		
		Locale.setDefault(Locale.US);
		
		List<Product> list = new ArrayList<>();
		
		list.add(new Product("Tv", 900.0));
		list.add(new Product("Mouse", 50.0));
		list.add(new Product("Tablet", 350.50));
		list.add(new Product("Hd case", 80.90));
		list.add(new Product("Computer", 850.00));
		list.add(new Product("Monitor", 290.00));
		
		double mean = list.stream().map(x -> x.getPrice()).reduce(0.0, (a, b) -> a + b) / list.size();
		
		System.out.println("Mean: " + String.format("%.2f", mean));
		System.out.println("Products above mean: ");
		list.stream()
		.filter(x -> x.getPrice() > mean)
		.sorted((x1, x2) -> x1.getName().toUpperCase().compareTo(x2.getName().toUpperCase()))
		.collect(Collectors.toList())
		.forEach(System.out::println);

	}

}
