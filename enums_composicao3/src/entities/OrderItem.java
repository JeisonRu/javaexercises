package entities;

public class OrderItem {

	private Integer quantity;
	
	private Product product;

	public OrderItem(Integer quantity, Product product) {
		this.quantity = quantity;
		this.product = product;
	}

	public Integer getQuantity() {
		return quantity;
	}

	public void increaseQuantity(Integer amount) {
		this.quantity += amount;
	}
	
	public void decreaseQuantity(Integer amount) {
		this.quantity -= amount;
	}
	
	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public Double subTotal() {
		return product.getPrice() * quantity;
	}
	
	
}
