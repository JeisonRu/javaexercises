package application;

import java.util.Locale;
import java.util.Scanner;

import entity.CurrencyConverter;

public class Program {

	public static void main(String[] args) {
		
		Locale.setDefault(Locale.US);
		Scanner sc = new Scanner(System.in);
		
		System.out.print("Qual � a conta��o atual do d�lar? ");
		double dolarValue = sc.nextDouble();
		System.out.print("Qual � o montante de d�lares a ser adquirido? ");
		double amount = sc.nextDouble();
		System.out.printf("Montante a ser pago em Reais = %.2f", 
				CurrencyConverter.custoDoDolarEmReais(dolarValue, amount));
		
		sc.close();

	}

}
