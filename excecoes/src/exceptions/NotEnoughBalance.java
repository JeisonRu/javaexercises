package exceptions;

public class NotEnoughBalance extends RuntimeException {
	private static final long serialVersionUID = 1L;

	public NotEnoughBalance(String msg) {
		super(msg);
	}
}
