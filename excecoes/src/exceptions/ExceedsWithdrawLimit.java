package exceptions;

public class ExceedsWithdrawLimit extends RuntimeException {
	private static final long serialVersionUID = 1L;

	public ExceedsWithdrawLimit(String msg) {
		super(msg);
	}
}
