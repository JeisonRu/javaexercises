package application;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class Program {

	public static void main(String[] args) {

		Locale.setDefault(Locale.US);
		
		List<String> lines = null;
		
		File path = new File("D:\\Users\\jeiso\\ws-eclipse\\arquivos");	
		File[] files = path.listFiles(File::isFile);
		File path2 = files[2];
		
		try (BufferedReader br = new BufferedReader(new FileReader(path2))){
			lines = new ArrayList<>();
			
			String line = br.readLine();
			
			while (line != null) {
				lines.add(line);
				line = br.readLine();
			}
 		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		new File(path + "\\out").mkdir();
		String newFile = "\\out\\summary.csv";
		new File(path + newFile);
		
		try (BufferedWriter bw = new BufferedWriter(new FileWriter(path + newFile))) {
			for (String line : lines) {
				String[] data = line.split(",");
				double subtotal = Double.parseDouble(data[1]) * Integer.parseInt(data[2]);
				bw.write(data[0] + "," + String.format("%.2f", subtotal));
				bw.newLine();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}

}
