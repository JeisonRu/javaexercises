package application;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Scanner;

import entities.Client;
import entities.Order;
import entities.OrderItem;
import entities.Product;
import entities.enums.OrderStatus;

public class Program {

	public static void main(String[] args) throws ParseException {
		
		Locale.setDefault(Locale.US);
		Scanner sc = new Scanner(System.in);
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		
		System.out.println("Enter client data: ");
		System.out.print("Name: ");
		String name = sc.nextLine();
		System.out.print("Email: ");
		String email = sc.next();
		System.out.print("Birth Date (DD/MM/YYYY): ");
		Date birth = sdf.parse(sc.next());
		Client client = new Client(name, email, birth);
		
		System.out.println("Enter order data: ");
		System.out.print("Status: ");
		OrderStatus status = OrderStatus.valueOf(sc.next());
		Order order = new Order(new Date(), status, client);
		
		System.out.print("How many items to this order? ");
		int n = sc.nextInt();
		for (int i = 1; i <= n; i++) {
			System.out.println("Enter #" + i + " item data:");
			System.out.print("Product name: ");
			String productName = sc.next();
			System.out.print("Product price: ");
			double productPrice = sc.nextDouble();
			System.out.print("Quantity: ");
			int quantity = sc.nextInt();
			Product p = new Product(productName, productPrice);
			OrderItem item = new OrderItem(quantity, p);
			order.addItem(item);
		}
		System.out.println();
		
		StringBuilder sb = new StringBuilder();
		sb.append("ORDER SUMMARY\n");
		sb.append("Order moment: " + sdf.format(order.getMoment()) + "\n");
		sb.append("Order status: " + order.getStatus().toString().
				substring(0, 1).toUpperCase() + 
				order.getStatus().toString().substring(1).toLowerCase() + "\n");
		sb.append("Client: " + order.getClient().getName() + " (" + 
				sdf.format(order.getClient().getBirthDate()) + ") - " + 
				order.getClient().getEmail() + "\n");
		sb.append("Order items:\n");
		for (OrderItem item : order.getItems()) {
			sb.append(item.getProduct().getName() + ", $" + 
					item.getProduct().getPrice() + ", ");
			sb.append("Quantity: " + item.getQuantity() + ", " +
					"Subtotal: $" + item.subTotal() + "\n");
		}
		sb.append("Total price: $" + order.total());
		System.out.println(sb.toString());

		sc.close();
	}

}
