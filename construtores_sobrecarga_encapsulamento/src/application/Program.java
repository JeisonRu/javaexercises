package application;

import java.util.Locale;
import java.util.Scanner;

import entity.Account;

public class Program {

	public static void main(String[] args) {
		
		Locale.setDefault(Locale.US);
		Scanner sc = new Scanner(System.in);
		
		Account acc;
		
		System.out.print("Informe n�mero da conta e nome para a abertura da conta: ");
		String account = sc.next();
		sc.nextLine();
		String name = sc.nextLine();			
		
		System.out.print("Haver� dep�sito inicial? (y/n) ");
		char option = sc.next().charAt(0);
		if (option == 'y' || option == 'Y') {
			System.out.print("Entre com o valor do dep�sito inicial: ");
			double balance = sc.nextDouble();
			acc = new Account(account, name, balance);
		} else {
			acc = new Account(account, name);
		}
				
		System.out.println(acc);
		System.out.println();
		
		System.out.print("Informe o valor a ser depositado: ");
		acc.deposit(sc.nextDouble());
		System.out.println(acc);
		System.out.println();
		
		System.out.print("Informe o valor a ser sacado: ");
		acc.withDraw(sc.nextDouble());
		System.out.println(acc);
		
		sc.close();

	}

}
