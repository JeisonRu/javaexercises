package entity;

public class Account {
	
	private String account;
	private String name;
	private double balance;
	private final double draftTax = 5.0;
	
	public Account(String account, String name, double initialDeposit) {
		this.account = account;
		this.name = name;
		deposit(initialDeposit);
	}

	public Account(String account, String name) {
		this.account = account;
		this.name = name;
	}

	public String getAccount() {
		return account;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getBalance() {
		return balance;
	}
	
	public void deposit(double amount) {
		this.balance += amount;
	}
	
	public void withDraw(double amount) {
		this.balance -= amount + draftTax;
	}

	@Override
	public String toString() {
		return "Account " + account + ", Holder: " + name + ", Balance: $ " + balance;
	}

}
